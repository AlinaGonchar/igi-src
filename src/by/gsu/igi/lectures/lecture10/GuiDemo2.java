package by.gsu.igi.lectures.lecture10;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created: 21.11.2016
 *
 * @author Evgeniy Myslovets
 */
public class GuiDemo2 implements ActionListener {

    private JButton button;

    public static void main(String[] args) {
        GuiDemo2 gui = new GuiDemo2();
        gui.go();
    }

    private void go() {
        JFrame frame = new JFrame();
        button = new JButton("Нажми на меня");

        button.addActionListener(this);

        frame.getContentPane().add(button);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        button.setText("На меня нажали!");
    }
}
