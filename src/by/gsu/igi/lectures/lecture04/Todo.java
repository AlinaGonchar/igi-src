package by.gsu.igi.lectures.lecture04;

public @interface Todo {
    String value();
}
