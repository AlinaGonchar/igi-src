package by.gsu.igi.lectures.lecture04;

public class PetOwner {

    public static void main(String[] args) {
        Animal dog = new Dog("Тузик");
        System.out.println(dog.toString());
        dog.makeSound();

        Animal cat = new Cat();
        cat.makeSound();

        Animal[] pets = new Animal[] {cat, dog};
        for (Animal pet : pets) {
            sound(pet);
            touch(pet);
        }

        Plant tree = new Plant();
        touch(tree);
//        System.out.println(new Bulldog("a") instanceof Animal);
//        System.out.println(new Dog("a") instanceof Animal);

        touch(new Touchable() {
            @Override
            public void touch() {
                System.out.println("Anonymous inner touchable");
            }
        });
    }

    public static void sound(Animal animal) {
        animal.makeSound();
    }

    public static void touch(Touchable animal) {
        animal.touch();
    }
}
