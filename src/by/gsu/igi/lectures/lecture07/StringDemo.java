package by.gsu.igi.lectures.lecture07;

/**
 * Created by Evgeniy Myslovets.
 */
public class StringDemo {

    public static void main(String[] args) {
        useString();

        useStringBuilder();
    }

    private static void useStringBuilder() {
        long start = System.nanoTime();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 100000; i++) {
            stringBuilder = stringBuilder.append(i);
        }
        long end = System.nanoTime();
        System.out.println(stringBuilder.toString());
        System.out.println((end - start) / 1000 / 1000);
    }

    private static void useString() {
        long start = System.nanoTime();
        String str = "";
        for (int i = 0; i < 10000; i++) {
            str += i;
        }
        long end = System.nanoTime();
        System.out.println(str);
        System.out.println((end - start) / 1000 / 1000);
    }
}
